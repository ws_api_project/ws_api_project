var mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
const timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var AttractionSchema = new Schema ({
  cityID: {type: mongoose.Schema.Types.ObjectId, ref: 'City'},
  name: {type: String, required: true},
  picture: {type: String, default: ""},
  latitude: Number,
  longitude: Number,
  address: String,
  description: {type: String, default: ""},
  avgRating: {type: Number, default: 1}
});

AttractionSchema.plugin(timestamps);
AttractionSchema.plugin(mongooseStringQuery);

// exporting the scheme for usage outside usage
var Attraction = mongoose.model('Attraction', AttractionSchema);
module.exports = Attraction;
