var mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
const timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    username : {type :String, required : true, unique : true}, //serves as unique identifier
    userPassword : {type : String, required: true},
    forename : {type : String, required : true},
    surname : {type : String, required: true},
    eMail: {type: String, required: true}
});

UserSchema.plugin(timestamps);
UserSchema.plugin(mongooseStringQuery);

var User = mongoose.model('User', UserSchema);
module.exports = User;
