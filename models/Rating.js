var mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
const timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var RatingSchema = new Schema ({
  rating: {type: Number, min: 1, max: 5, required: true}
});

RatingSchema.plugin(timestamps);
RatingSchema.plugin(mongooseStringQuery);

// exporting the scheme for usage outside usage
var Rating = mongoose.model('Rating', RatingSchema);
module.exports = Rating;
