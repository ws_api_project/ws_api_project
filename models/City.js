var mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
const timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var CitySchema = new Schema ({
  name: String,
  countryCode: String,
  latitude: Number,
  longitude: Number,
});

CitySchema.plugin(timestamps);
CitySchema.plugin(mongooseStringQuery);

// exporting the scheme for usage outside usage
var City = mongoose.model('City', CitySchema);
module.exports = City;
