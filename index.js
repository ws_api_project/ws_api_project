
/**
 * Module Dependencies
 */
const config = require('./config');
const restify = require('restify');
const mongoose = require('mongoose');
const restifyPlugins = require('restify-plugins');
const jwt = require('jsonwebtoken');
//const timestamp = require('mongoose-timestamp');
//var swagger = require('swagger-restify');
/**
  * Initialize Server
  */
var server = restify.createServer({
	name: config.name,
	version: config.version,
});
server.pre(restify.pre.sanitizePath());

//not sure if needed for Heroku or not -> test!
function respond(req, res, next) {
    res.send('Hello Heroku User!');
}

server.get('/', respond);
//not sure if needed for Heroku or not -> test!

/**
  * Middleware
  */
server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());


/**
  * Start Server, Connect to DB & Require Routes
  */
server.listen(config.port, function () {
	//establish connection to mongodb
	mongoose.Promise = global.Promise;
	mongoose.connect(config.db.uri, { useMongoClient: true });

	const db = mongoose.connection;

	db.on('error', function (err) {
	    console.error(err);
	    process.exit(1);
	});

	db.once('open', function () {
	    require('./routes/city')(server);
	    require('./routes/attraction')(server);
	    require('./routes/rating')(server);
	    require('./routes/user')(server);
	    console.log(`Server is listening on port ${config.port}`);
	});
});
