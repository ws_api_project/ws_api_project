## ToDo
## clean mongo and Heroko etc

# ws_api_project

This is the API for the WebServices Project at UPC 2017 using NodeJs and Express 4 using [Express 4](http://expressjs.com/).

## Swagger Documentation
https://app.swaggerhub.com/apis/WS-UPC-17/WebServicesProject-UPC-17/1.0.0#

## Online access
https://ws-api-project.herokuapp.com/

## Online running mongoDB instance on
https://mlab.com/databases/ws_api_project
[no local instance needed]

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) installed.

```sh
$ git clone https://themerowinger@bitbucket.org/ws_api_project/ws_api_project.git # or clone your own fork
$ cd ws_api_project
$ npm install
$ npm start
```

Your app should now be running on [localhost:3000](http://localhost:3000/).
