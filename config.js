module.exports = {
	name: 'API',
	secret: 'secretservice',
	env: process.env.NODE_ENV || 'development',
	port: process.env.PORT || 3000,
	base_url: process.env.BASE_URL,
	db: {
		uri: process.env.MONGODB_URI || 'mongodb://dataUser:M421TUMher0n@ds139436.mlab.com:39436/ws_api_project',
	},
};

//|| 'http://localhost:3000'
//local mongo mongodb://127.0.0.1:27017/api
