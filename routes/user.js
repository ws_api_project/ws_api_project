
const config = require('../config.js');
/**
 * Module Dependencies
 */
const errors = require('restify-errors');
/**
 * Model Schema
 */
const User = require('../models/user');
/**
 * Object JSON Web Token
 */
var jwt = require('jsonwebtoken');

module.exports = function(server) {

  /**
   * authenticate a user
   */
	server.post('/users/login', (req, res, next) => {
    // parse body
		let jsonBody = null;
    try {
    	jsonBody = JSON.parse(req.body);
		} catch (e) {
			jsonBody = req.body;
		}

	  // find the user
	  User.findOne({
	    username: jsonBody.username
	  }, function(err, user) {
	    if (err) throw err;

	    if (!user) {
	      res.json({ success: false, message: 'Authentication failed. User not found.' });
	    } else if (user) {

	      // check if password matches
	      if (user.userPassword != jsonBody.userPassword) {
	        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
	      } else {

	        // if user is found and password is right
	        // create a token with only our given payload
			    // we don't want to pass in the entire user since that has the password
			    const payload = {
			      username: user.username
			    };
	        var token = jwt.sign(payload, config.secret
          , {
	          expiresIn: Math.floor(Date.now() / 1000) + (20 * 60) // valid for 20 m
	        });

	        // return the information including token as JSON
	        res.json({
	          success: true,
	          message: 'Enjoy your token!',
	          token: token
	        });
	      }
	    }
	  });
    next();
	})


	/**
	 * Create
	 */
	server.post('/users', (req, res, next) => {
    if (!req.is('application/json')) {
      return next(
        new errors.InvalidContentError("Expects 'application/json'"),
      );
    }

    let data = req.body || {};

    let user = new User(data);
    user.save(function(err) {
      if (err) {
        console.error(err);
        return next(new errors.InternalError(err.message));
      }
      res.send(201, data);
      next();
    });
	})

	/**
	 * List all users -> only for dev and showcase purposes
	 * would not be available in production!
	 */
	server.get('/users', (req, res, next) => {
            User.apiQuery(req.params, function(err, docs) {
              if (err) {
                console.error(err);
                //return next();
              }

              res.send(docs);
              next();
            });
        });

	/**
	 * Find User by userID
	 */
	server.get('/users/:userID', (req, res, next) => {
            User.findOne({ _id: req.params.userID }, function(err, doc) {
        			if (err) {
        				console.error(err);
        				//return next();
        			}

        			res.send(doc);
        		});
          next();
        });

	/**
	 * Update user by userID
	 */
	server.put('/users/:userID', (req, res, next) => {
    // authorization check
    var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

          if (!req.is('application/json')) {
      			return next(
      				new errors.InvalidContentError("Expects 'application/json'"),
      			);
      		}

      		let data = req.body || {};

      		if (!data._id) {
      			data = Object.assign({}, data, { _id: req.params.userID });
      		}

      		User.findOne({ _id: req.params.userID }, function(err, doc) {
      			if (err) {
      				console.error(err);
      				return next(
      					new errors.InvalidContentError(err.errors.name.message),
      				);
      			} else if (!doc) {
      				return next(
      					new errors.ResourceNotFoundError(
      						'The resource you requested could not be found.',
      					),
      				);
      			}

      			User.update({ _id: data._id }, data, function(err) {
      				if (err) {
      					console.error(err);
      					return next(
      						new errors.InvalidContentError(err.errors.name.message),
      					);
      				}
      				res.send(200, data);
      			});
      		});
        }
      });
    } else {
      // if there is no token return an error
      return res.send(403, {
          success: false, message: 'No token provided.'
      });
    }
	})

	/**
	 * Delete user by userID
	 */
	server.del('/users/:userID', (req, res, next) => {
    // authorization check
    var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

          User.remove({ _id: req.params.userID }, function(err) {
      			if (err) {
      				console.error(err);
      				return next(
      					new errors.InvalidContentError(err.errors.name.message),
      				);
      			}
      			res.send(204);
      		});
        }
      });
    } else {
      // if there is no token return an error
      return res.send(403, {
          success: false, message: 'No token provided.'
      });
    }
    next();
	})
}
