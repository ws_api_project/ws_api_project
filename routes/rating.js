
const config = require('../config.js');
/**
 * Module Dependencies
 */
const errors = require('restify-errors');
/**
 * Model Schema
 */
const Rating = require('../models/Rating');
const Attraction = require('../models/Attraction');
const City = require('../models/City');
/**
 * Object JSON Web Token
 */
var jwt = require('jsonwebtoken');

module.exports = function(server) {

	/**
	 * POST
	 */
	server.post('/cities/:cityID/attractions/:attractionID/ratings', (req, res, next) => {
		var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}


		let cityID = req.params.cityID;
		let attractionID = req.params.attractionID;
		console.log(cityID);
		console.log(attractionID);

			let dataRating = req.body || {};
			//assignes the provided cityID and attractionID from the URL to the posted rating data
			dataRating = Object.assign({}, dataRating, { cityID: cityID }, {attractionID: attractionID});

			let rating = new Rating(dataRating);
			rating.save(function(err) {
				if (err) {
					console.error(err);
					return next(new errors.InternalError(err.message));
					next();
				}
				//console.log(dataRating._id);
				console.log(dataRating);
				res.send(201, dataRating);
				next();
			});
		}
		});
		} else {
				// if there is no token return an error
				return res.send(403, {
				success: false, message: 'No token provided.'
				});
			}
	});


	/**
	 * LIST all ratings of a specific attraction
	 */
	server.get('/cities/:cityID/attractions/:attractionID/ratings', (req, res, next) => {

		Rating.apiQuery( {attractionID: req.params.attractionID}, function(err, docs) {

			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(docs);
			next();
		});
	//});
	});

	/**
	 * GET specific rating
	 */
	server.get('/cities/:cityID/attractions/:attractionID/ratings/:ratingID', (req, res, next) => {
		//no cityID and attractionID needed here since they are anyways bound from post?
		Rating.findOne({ cityID: req.params.cityID, attractionID: req.params.attractionID, _id: req.params.ratingID}, function(err, doc) {
			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(doc);
			next();
		});
	});

	/**
	 * UPDATE
	 */
	server.put('/cities/:cityID/attractions/:attractionID/ratings/:_id', (req, res, next) => {
		var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}

		let data = req.body || {};

		if (!data._id) {
			data = Object.assign({}, data, { _id: req.params._id });
		}

		Rating.findOne({ _id: req.params._id }, function(err, doc) {
			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			} else if (!doc) {
				return next(
					new errors.ResourceNotFoundError(
						'The resource you requested could not be found.',
					),
				);
			}

			Rating.update({ _id: data._id }, data, function(err) {
				if (err) {
					console.error(err);
					return next(
						//new errors.InvalidContentError(err.errors.name.message),
					);
				}

				res.send(200, data);
				next();
			});
		});
	}
});
} else {
// if there is no token return an error
return res.send(403, {
		success: false, message: 'No token provided.'
});
}
});


	/**
	 * DELETE
	 */
	server.del('/cities/:cityID/attractions/:attractionID/ratings/:_id', (req, res, next) => {
		var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

					Rating.remove({ _id: req.params._id }, function(err) {
						if (err) {
							console.error(err);
							return next(
								//new errors.InvalidContentError(err.errors.name.message),
							);
						}

						res.send(204);
						next();
					});
        }
      });
    } else {
      // if there is no token return an error
      return res.send(403, {
          success: false, message: 'No token provided.'
      });
    }
	});

	
};
