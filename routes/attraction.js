
const config = require('../config.js');
/**
 * Module Dependencies
 */
const errors = require('restify-errors');
/**
 * Model Schema
 */
const Attraction = require('../models/Attraction');
const City = require('../models/City');
/**
 * Object JSON Web Token
 */
var jwt = require('jsonwebtoken');

module.exports = function(server) {

	/**
	 * POST
	 */
	server.post('/cities/:cityID/attractions', (req, res, next) => {
		var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}


		let cityID = req.params.cityID;

		console.log(cityID);

			let dataAttr = req.body || {};
			//assignes the provided cityID from the URL to the posted attraction data
			dataAttr = Object.assign({}, dataAttr, { cityID: cityID });

			let attraction = new Attraction(dataAttr);
			attraction.save(function(err) {
				if (err) {
					console.error(err);
					return next(new errors.InternalError(err.message));
					next();
				}
				//console.log(dataAttr._id);
				console.log(dataAttr);
				res.send(201, dataAttr);
				next();
			});
		//});
	}
});
} else {
// if there is no token return an error
return res.send(403, {
		success: false, message: 'No token provided.'
});
}
});

	/**
	 * LIST all attractions of a specified city
	 */
	server.get('/cities/:cityID/attractions', (req, res, next) => {

		Attraction.apiQuery( {cityID: req.params.cityID}, function(err, docs) {

			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(docs);
			next();
		});
	});

	/**
	 * GET specific attraction
	 */
	server.get('/cities/:cityID/attractions/:_id', (req, res, next) => {
		Attraction.findOne({ cityID: req.params.cityID, _id: req.params._id}, function(err, doc) {
			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(doc);
			next();
		});
	});


	/**
	 * UPDATE
	 */
	server.put('/cities/:cityID/attractions/:_id', (req, res, next) => {
		var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}

		let data = req.body || {};

		if (!data._id) {
			data = Object.assign({}, data, { _id: req.params._id });
		}

		Attraction.findOne({ _id: req.params._id }, function(err, doc) {
			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			} else if (!doc) {
				return next(
					new errors.ResourceNotFoundError(
						'The resource you requested could not be found.',
					),
				);
			}

			Attraction.update({ _id: data._id }, data, function(err) {
				if (err) {
					console.error(err);
					return next(
						//new errors.InvalidContentError(err.errors.name.message),
					);
				}

				res.send(200, data);
				next();
			});
		});
	}
});
} else {
// if there is no token return an error
return res.send(403, {
		success: false, message: 'No token provided.'
});
}
});

	/**
	 * DELETE
	 */
	server.del('/cities/:cityID/attractions/:_id', (req, res, next) => {
		var token = req.headers.authorization;
    if (token) {
      token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;

					Attraction.remove({ _id: req.params._id }, function(err) {
						if (err) {
							console.error(err);
							return next(
								//new errors.InvalidContentError(err.errors.name.message),
							);
						}

						res.send(204);
						next();
					});
        }
      });
    } else {
      // if there is no token return an error
      return res.send(403, {
          success: false, message: 'No token provided.'
      });
    }
	});


	/**
	 * retrieves top ten attractions of a specific city
	 */
	server.get('/cities/:cityID/retrievePopularAttractions', (req, res, next) => {

		let limit = parseInt(req.query.limit, 10) || 10, // default limit to 10 docs
						skip  = parseInt(req.query.skip, 10) || 0, // default skip to 0 docs
						query = req.query || {}

				// remove skip and limit from query to avoid false querying
				delete query.skip
				delete query.limit

			Attraction.find(query).sort('-avgRating').skip(skip).limit(limit)
			.then(attractions => {
				res.send(200, attractions)
				next()
			})
			.catch(err => {
				res.send(500, err)
			})
	});


};
