
const config = require('../config.js');
/**
 * Module Dependencies
 */
const errors = require('restify-errors');
/**
 * Model Schema
 */
const City = require('../models/City');
/**
 * Object JSON Web Token
 */
var jwt = require('jsonwebtoken');

module.exports = function(server) {
// var baseURL = "/cities";
	// /**
	//  * POST not available in the API just for dev purposes
	//  * and data input -> SOBI data injection
	//  */
	// server.post('/cities', (req, res, next) => {
	// 	if (!req.is('application/json')) {
	// 		return next(
	// 			new errors.InvalidContentError("Expects 'application/json'"),
	// 		);
	// 	}
  //
	// 	let data = req.body || {};
  //
	// 	let city = new City(data);
	// 	city.save(function(err) {
	// 		if (err) {
	// 			console.error(err);
	// 			return next(new errors.InternalError(err.message));
	// 			next();
	// 		}
  //
	// 		res.send(201, data);
	// 		next();
	// 	});
	// });

	/**
	 * LIST all cities
	 */
	server.get('/cities', (req, res, next) => {
		City.apiQuery(req.params, function(err, docs) {
			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(docs);
			next();
		});
	});

	/**
	 * GET City by cityID
	 */
	server.get('/cities/:cityID', (req, res, next) => {
		City.findOne({ _id: req.params.cityID }, function(err, doc) {
			if (err) {
				console.error(err);
				return next(
					//new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(doc);
			next();
		});
	});

	// /**
	// * UPDATE not available in the API just for dev purposes
  // * and data input -> SOBI data injection
	//  */
	// server.put('/cities/:cityID', (req, res, next) => {
	// 	if (!req.is('application/json')) {
	// 		return next(
	// 			new errors.InvalidContentError("Expects 'application/json'"),
	// 		);
	// 	}
  //
	// 	let data = req.body || {};
  //
	// 	if (!data._id) {
	// 		data = Object.assign({}, data, { _id: req.params.cityID });
	// 	}
  //
	// 	City.findOne({ _id: req.params.cityID }, function(err, doc) {
	// 		if (err) {
	// 			console.error(err);
	// 			return next(
	// 				new errors.InvalidContentError(err.errors.name.message),
	// 			);
	// 		} else if (!doc) {
	// 			return next(
	// 				new errors.ResourceNotFoundError(
	// 					'The resource you requested could not be found.',
	// 				),
	// 			);
	// 		}
  //
	// 		City.update({ _id: data._id }, data, function(err) {
	// 			if (err) {
	// 				console.error(err);
	// 				return next(
	// 					new errors.InvalidContentError(err.errors.name.message),
	// 				);
	// 			}
  //
	// 			res.send(200, data);
	// 			next();
	// 		});
	// 	});
	// });

// 	/**
// 	* DELETE not available in the API just for dev purposes
//   * and data input -> SOBI data injection
// 	*/
// 	server.del('/cities/:cityID', (req, res, next) => {
// 		City.remove({ _id: req.params.cityID }, function(err) {
// 			if (err) {
// 				console.error(err);
// 				return next(
// 					new errors.InvalidContentError(err.errors.name.message),
// 				);
// 			}
//
// 			res.send(204);
// 			next();
// 		});
// 	});
};
